# xpipeline-waveforms

This houses the LIGO Data Format compliant, pre generated waveforms that can be used with the pythonic version of xpipeline. 
The GW strain data for many of the SN wavforms were originally found in Jade Powell's well curated [repository](https://git.ligo.org/bursts/supernova/waveforms).
(LIGO authentication needed).
There is a script in this repo that shows how I resampled, and detrended the waveforms in that repository for use in X-Pypeline waveform injection scheme.