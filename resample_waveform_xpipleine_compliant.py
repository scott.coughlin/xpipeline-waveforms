from gwpy.timeseries import TimeSeries
from scipy.interpolate import interp1d

import numpy
import h5py
import os
import pandas
import math

# file names
sn_strains = ['Powell_2018/Powell_s18.h5', 'Powell_2018/Powell_he3pt5.h5']

for strain_file in sn_strains:

    # read in file
    f = h5py.File(strain_file, 'r')
    waveform = numpy.array(f['waveform'])
    f.close()
    sn_name = strain_file.split('/')[-1].split('_')[-1].split('.')[0]
    hp = TimeSeries(waveform[:,1], xindex=waveform[:,0])
    hc = TimeSeries(waveform[:,2], xindex=waveform[:,0])

    # make sure time series starts at 0
    hp.xindex = hp.xindex - hp.xindex[0]
    hc.xindex = hc.xindex - hc.xindex[0]

    # perform a linear detrending
    hp = hp - (((hp.value[-1] - hp.value[0])/(hp.xindex[-1].value - hp.xindex[0].value))*hp.xindex.value + hp.value[0])
    hc = hc - (((hc.value[-1] - hc.value[0])/(hc.xindex[-1].value - hc.xindex[0].value))*hc.xindex.value + hc.value[0])

    # Let's way over sample this waveform before resample back down to a LIGO friendly value
    new_x_hp = numpy.arange(0, hp.xindex[-1].value - 1./65536, 1./65536)
    new_x_hc = numpy.arange(0, hc.xindex[-1].value - 1./65536, 1./65536)

    function_to_get_new_hp = interp1d(hp.xindex, hp)
    function_to_get_new_hc = interp1d(hc.xindex, hc)

    new_hp = function_to_get_new_hp(new_x_hp)
    new_hc = function_to_get_new_hc(new_x_hc)

    new_hp = TimeSeries(new_hp, name='{0}'.format(sn_name), dx=1./65536)
    new_hc = TimeSeries(new_hc, name='{0}'.format(sn_name), dx=1./65536)

    # resample to a LIGO friendly analyis pipeline friendly value of 16384
    new_hp = new_hp.resample(16384)
    new_hc = new_hc.resample(16384)

    # Now we zero pad to 1 (or power of two) seconds because X likes that
    duration_desired = int(numpy.ceil(new_hp.duration).value)

    hp_zeros = TimeSeries(numpy.zeros(duration_desired * 16384), dx=1./16384, name='{0}'.format(sn_name))
    hc_zeros = TimeSeries(numpy.zeros(duration_desired * 16384), dx=1./16384, name='{0}'.format(sn_name))

    new_hc = hc_zeros.inject(new_hc)
    new_hp = hp_zeros.inject(new_hp)

    # plot orginal and new waveform make sure things are not crazy
    plot = new_hp.plot()
    ax = plot.gca()
    ax.plot(new_hc)
    ax.plot(hp)
    ax.plot(hc)

    # currently scaled to distance of one centimeter? Lets make that 1 kiloparsec
    new_hp = new_hp*1./3.08567758128e21
    new_hc = new_hc*1./3.08567758128e21

    # Save plot
    plot.savefig('/home/scoughlin/public_html/PhD/Project/LIGO/BURST/xpipeline/sn/Powell2018_{0}.png'.format(sn_name))

    new_hp.write('Powell2018.hdf5', path='/Powell2018/{0}/hp/'.format(sn_name), append=True)
    new_hc.write('Powell2018.hdf5', path='/Powell2018/{0}/hc/'.format(sn_name), append=True)


# Kuroda_2017
# file names
sn_strains = ['Kuroda_2017/S15.0_GW_Nu.h5', 'Kuroda_2017/S11.2_GW_Nu.h5']

for strain_file in sn_strains:

    f = h5py.File(strain_file, 'r')
    waveform = numpy.array(f['waveform'])
    f.close()
    sn_name = strain_file.split('/')[-1].split('.h')[0]

    # make sure time series starts at 0
    if waveform[0,0] < 0:
        hp = TimeSeries(waveform[:,1], xindex=waveform[:,0] + abs(waveform[0,0]))
        hc = TimeSeries(waveform[:,2], xindex=waveform[:,0] + abs(waveform[0,0]))
    else:
        hp = TimeSeries(waveform[:,1], xindex=waveform[:,0] - waveform[0,0])
        hc = TimeSeries(waveform[:,2], xindex=waveform[:,0] - waveform[0,0])

    # make sure time series starts at 0
    hp.xindex = hp.xindex - hp.xindex[0]
    hc.xindex = hc.xindex - hc.xindex[0]

   # perform a linear detrending
    hp = hp - (((hp.value[-1] - hp.value[0])/(hp.xindex[-1].value - hp.xindex[0].value))*hp.xindex.value + hp.value[0])
    hc = hc - (((hc.value[-1] - hc.value[0])/(hc.xindex[-1].value - hc.xindex[0].value))*hc.xindex.value + hc.value[0])

    # Let's way over sample this waveform before resample back down to a LIGO friendly value
    new_x_hp = numpy.arange(0, hp.xindex[-1].value - 1./65536, 1./65536)
    new_x_hc = numpy.arange(0, hc.xindex[-1].value - 1./65536, 1./65536)

    function_to_get_new_hp = interp1d(hp.xindex, hp)
    function_to_get_new_hc = interp1d(hc.xindex, hc)

    new_hp = function_to_get_new_hp(new_x_hp)
    new_hc = function_to_get_new_hc(new_x_hc)

    new_hp = TimeSeries(new_hp, name='{0}'.format(sn_name), dx=1./65536)
    new_hc = TimeSeries(new_hc, name='{0}'.format(sn_name), dx=1./65536)

    # resample to a LIGO friendly analyis pipeline friendly value of 4096 respecting the nyquist frequency of the orginal waveform
    new_hp = new_hp.resample(4096)
    new_hc = new_hc.resample(4096)

    # Now we zero pad to 1 (or power of two) seconds because X likes that
    duration_desired = int(numpy.ceil(new_hp.duration).value)

    hp_zeros = TimeSeries(numpy.zeros(duration_desired * 4096), dx=1./4096, name='{0}'.format(sn_name))
    hc_zeros = TimeSeries(numpy.zeros(duration_desired * 4096), dx=1./4096, name='{0}'.format(sn_name))

    new_hc = hc_zeros.inject(new_hc)
    new_hp = hp_zeros.inject(new_hp)

    plot = new_hp.plot()
    ax = plot.gca()
    ax.plot(new_hc)
    ax.plot(hp)
    ax.plot(hc)

    # currently scaled to distance of one centimeter? Lets make that 1 kiloparsec
    new_hp = new_hp*1./3.08567758128e21
    new_hc = new_hc*1./3.08567758128e21

    # Save plot
    plot.savefig('/home/scoughlin/public_html/PhD/Project/LIGO/BURST/xpipeline/sn/Kuroda2017_{0}.png'.format(sn_name))

    new_hp.write('Kuroda2017.hdf5', path='/Kuroda2017/{0}/hp/'.format(sn_name), append=True)
    new_hc.write('Kuroda2017.hdf5', path='/Kuroda2017/{0}/hc/'.format(sn_name), append=True)

# file names
sn_strains = ['Yakunin_2017/C15-3D_GWsignal_0-438ms.h5']

for strain_file in sn_strains:

    f = h5py.File(strain_file, 'r')
    waveform = numpy.array(f['waveform'])
    f.close()
    sn_name = strain_file.split('/')[-1].split('.h')[0]

    # make sure time series starts at 0
    if waveform[0,0] < 0:
        hp = TimeSeries(waveform[:,1], xindex=(waveform[:,0] + abs(waveform[0,0])))
        hc = TimeSeries(waveform[:,2], xindex=(waveform[:,0] + abs(waveform[0,0])))
    else:
        hp = TimeSeries(waveform[:,1], xindex=(waveform[:,0] - waveform[0,0]))
        hc = TimeSeries(waveform[:,2], xindex=(waveform[:,0] - waveform[0,0]))

    # make sure time series starts at 0
    hp.xindex = hp.xindex - hp.xindex[0]
    hc.xindex = hc.xindex - hc.xindex[0]

    hp = hp - (((hp.value[-1] - hp.value[0])/(hp.xindex[-1].value - hp.xindex[0].value))*hp.xindex.value + hp.value[0])
    hc = hc - (((hc.value[-1] - hc.value[0])/(hc.xindex[-1].value - hc.xindex[0].value))*hc.xindex.value + hc.value[0])

    # The waveform is alomst sampled at 20kHz so lets made a time series
    # as such with a one sample left off at the end because the sampling
    # in the text file is not perfect

    new_x_hp = numpy.arange(0, hp.xindex[-1].value - 1./65536, 1./65536)
    new_x_hc = numpy.arange(0, hc.xindex[-1].value - 1./65536, 1./65536)

    function_to_get_new_hp = interp1d(hp.xindex, hp)
    function_to_get_new_hc = interp1d(hc.xindex, hc)

    new_hp = function_to_get_new_hp(new_x_hp)
    new_hc = function_to_get_new_hc(new_x_hc)

    new_hp = TimeSeries(new_hp, name='{0}'.format(sn_name), dx=1./65536)
    new_hc = TimeSeries(new_hc, name='{0}'.format(sn_name), dx=1./65536)

    new_hp = new_hp.resample(4096)
    new_hc = new_hc.resample(4096)

    # Now we zero pad
    duration_desired = int(numpy.ceil(new_hp.duration).value)

    hp_zeros = TimeSeries(numpy.zeros(duration_desired * 4096), dx=1./4096, name='{0}'.format(sn_name))
    hc_zeros = TimeSeries(numpy.zeros(duration_desired * 4096), dx=1./4096, name='{0}'.format(sn_name))

    new_hc = hc_zeros.inject(new_hc)
    new_hp = hp_zeros.inject(new_hp)

    plot = new_hp.plot()
    ax = plot.gca()
    ax.plot(new_hc)
    ax.plot(hp)
    ax.plot(hc)

    # currently scaled to distance of 10 kiloparsec. Lets make that 1 kiloparsec
    new_hp = new_hp*1./10
    new_hc = new_hc*1./10

    plot.savefig('/home/scoughlin/public_html/PhD/Project/LIGO/BURST/xpipeline/sn/Yakunin2017_{0}.png'.format(sn_name))

    # save the resmapled waveforms
    new_hp.write('Yakunin2017.hdf5', path='/Yakunin2017/{0}/hp/'.format(sn_name), append=True)
    new_hc.write('Yakunin2017.hdf5', path='/Yakunin2017/{0}/hc/'.format(sn_name), append=True)

sn_strains = ['mesa20_gw.dat', 'mesa20_pert_gw.dat', 'mesa20_LR_gw.dat', 'mesa20_pert_LR_gw.dat', 'mesa20_v_LR_gw.dat']

for strain_file in sn_strains:

    waveform = pandas.read_csv(os.path.join('Oconnor_Couch_2018','oconnorcouch2018b_gwdata', strain_file), sep=" ", header=None)
    sn_name = strain_file.split('_gw.dat')[0]

    # make sure time series starts at 0
    times = waveform[0].values
    hp = waveform[7]
    hc = waveform[8]
    if times[0] < 0:
        hp = TimeSeries(hp, xindex=times + abs(times[0]))
        hc = TimeSeries(hc, xindex=times + abs(times[0]))
    else:
        hp = TimeSeries(hp, xindex=times - abs(times[0]))
        hc = TimeSeries(hc, xindex=times - abs(times[0]))

    hp = hp - (((hp.value[-1] - hp.value[0])/(hp.xindex[-1].value - hp.xindex[0].value))*hp.xindex.value + hp.value[0])
    hc = hc - (((hc.value[-1] - hc.value[0])/(hc.xindex[-1].value - hc.xindex[0].value))*hc.xindex.value + hc.value[0])

    # The waveform is alomst sampled at 20kHz so lets made a time series
    # as such with a one sample left off at the end because the sampling
    # in the text file is not perfect

    new_x_hp = numpy.arange(0, hp.xindex[-1].value - 1./65536, 1./65536)
    new_x_hc = numpy.arange(0, hc.xindex[-1].value - 1./65536, 1./65536)

    function_to_get_new_hp = interp1d(hp.xindex, hp)
    function_to_get_new_hc = interp1d(hc.xindex, hc)

    new_hp = function_to_get_new_hp(new_x_hp)
    new_hc = function_to_get_new_hc(new_x_hc)

    new_hp = TimeSeries(new_hp, name='{0}'.format(sn_name), dx=1./65536)
    new_hc = TimeSeries(new_hc, name='{0}'.format(sn_name), dx=1./65536)

    new_hp = new_hp.resample(16384)
    new_hc = new_hc.resample(16384)

    # Now we zero pad
    duration_desired = int(numpy.ceil(new_hp.duration).value)

    hp_zeros = TimeSeries(numpy.zeros(duration_desired * 16384), dx=1./16384,name='{0}'.format(sn_name))
    hc_zeros = TimeSeries(numpy.zeros(duration_desired * 16384), dx=1./16384,name='{0}'.format(sn_name))

    new_hc = hc_zeros.inject(new_hc)
    new_hp = hp_zeros.inject(new_hp)

    plot = new_hp.plot()
    ax = plot.gca()
    ax.plot(new_hc)
    ax.plot(hp)
    ax.plot(hc)

    # currently scaled to distance of one centimeter? Lets make that 1 kiloparsec
    new_hp = new_hp*1./3.08567758128e21
    new_hc = new_hc*1./3.08567758128e21

    plot.savefig('/home/scoughlin/public_html/PhD/Project/LIGO/BURST/xpipeline/sn/Oconnor_Couch_2018_{0}.png'.format(sn_name))

    new_hp.write('OconnorCouch2018.hdf5', path='/OconnorCouch2018/{0}/hp/'.format(sn_name), append=True)
    new_hc.write('OconnorCouch2018.hdf5', path='/OconnorCouch2018/{0}/hc/'.format(sn_name), append=True)

# Richers
f = h5py.File('Richers_2017/GWdatabase.h5','r')
waveforms = f['waveforms'].keys()

for waveform in waveforms:

    strain = f['waveforms'][waveform]['strain*dist(cm)'][:]
    sn_name = waveform
    time = f['waveforms'][waveform]['t-tb(s)'][:] 

    # make sure time series starts at 0
    if time[0] < 0:
        hp = TimeSeries(strain, xindex=(time + abs(time[0])))
    else:
        hp = TimeSeries(strain, xindex=(time - time[0]))

    # make sure time series starts at 0
    hp.xindex = hp.xindex - hp.xindex[0]

    hp = hp - (((hp.value[-1] - hp.value[0])/(hp.xindex[-1].value - hp.xindex[0].value))*hp.xindex.value + hp.value[0])

    # The waveform is alomst sampled at 20kHz so lets made a time series
    # as such with a one sample left off at the end because the sampling
    # in the text file is not perfect

    new_x_hp = numpy.arange(0, hp.xindex[-1].value - 1./65536, 1./65536)

    function_to_get_new_hp = interp1d(hp.xindex, hp)

    new_hp = function_to_get_new_hp(new_x_hp)

    new_hp = TimeSeries(new_hp, name='{0}'.format(sn_name), dx=1./65536)

    new_hp = new_hp.resample(16384)

    # Now we zero pad
    duration_desired = int(numpy.ceil(new_hp.duration).value)

    hp_zeros = TimeSeries(numpy.zeros(duration_desired * 16384), dx=1./16384, name='{0}'.format(sn_name))

    new_hp = hp_zeros.inject(new_hp)

    # currently scaled to distance of one centimeter? Lets make that 1 kiloparsec
    new_hp = new_hp*1./3.08567758128e21

    new_hp.write('Richers2017.hdf5', path='/Richers2017/{0}/hp/'.format(sn_name), append=True)

# file names
sn_strains = ['./Morozova_2018/gwstrain_M10_LS220.h5', './Morozova_2018/gwstrain_M10_SFHo.h5',
              './Morozova_2018/gwstrain_M13_SFHo_multipole.h5', './Morozova_2018/gwstrain_M19_SFHo.h5',
              './Morozova_2018/gwstrain_M10_DD2.h5', './Morozova_2018/gwstrain_M10_LS220_no_manybody.h5',
              './Morozova_2018/gwstrain_M13_SFHo_rotating.h5', './Morozova_2018/gwstrain_M13_SFHo.h5']

for strain_file in sn_strains:

    f = h5py.File(strain_file, 'r')
    waveform = numpy.array(f['waveform'])
    f.close()
    sn_name = strain_file.split('/')[-1].split('.h')[0].lstrip('gwstrain_')

    # make sure time series starts at 0
    if waveform[0,0] < 0:
        hp = TimeSeries(waveform[:,1], xindex=(waveform[:,0] + abs(waveform[0,0])))
    else:
        hp = TimeSeries(waveform[:,1], xindex=(waveform[:,0] - waveform[0,0]))

    # make sure time series starts at 0
    hp.xindex = hp.xindex - hp.xindex[0]

    hp = hp - (((hp.value[-1] - hp.value[0])/(hp.xindex[-1].value - hp.xindex[0].value))*hp.xindex.value + hp.value[0])

    # The waveform is alomst sampled at 20kHz so lets made a time series
    # as such with a one sample left off at the end because the sampling
    # in the text file is not perfect

    new_x_hp = numpy.arange(0, hp.xindex[-1].value - 1./65536, 1./65536)

    function_to_get_new_hp = interp1d(hp.xindex, hp)

    new_hp = function_to_get_new_hp(new_x_hp)

    new_hp = TimeSeries(new_hp, name='{0}'.format(sn_name), dx=1./65536)

    new_hp = new_hp.resample(16384)

    # Now we zero pad to duration of power of two
    next_power_of_two = int(math.pow(2, math.ceil(math.log(new_hp.duration.value)/math.log(2))))

    hp_zeros = TimeSeries(numpy.zeros(next_power_of_two * 16384), dx=1./16384, name='{0}'.format(sn_name))

    new_hp = hp_zeros.inject(new_hp)

    plot = new_hp.plot()
    ax = plot.gca()
    ax.plot(hp)

    # currently scaled to distance of one centimeter? Lets make that 1 kiloparsec
    new_hp = new_hp**1./3.08567758128e21

    plot.savefig('/home/scoughlin/public_html/PhD/Project/LIGO/BURST/xpipeline/sn/Morozova_{0}.png'.format(sn_name))

    # save the resmapled waveforms
    new_hp.write('Morozova2018.hdf5', path='/Morozova2018/{0}/hp/'.format(sn_name), append=True)
